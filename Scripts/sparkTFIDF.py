from pyspark.sql import SparkSession
from pyspark.ml.feature import Tokenizer, HashingTF, IDF
from pyspark.ml import Pipeline

# Initialize a Spark session
spark = SparkSession.builder \
    .appName("TF-IDF Example") \
    .getOrCreate()

# Load your data into a DataFrame (assuming CSV format for this example)
df = spark.read.csv("emails.csv", header=True, inferSchema=True)

# Tokenize the text
tokenizer = Tokenizer(inputCol="body", outputCol="words")

# Compute Term Frequency
hashingTF = HashingTF(inputCol="words", outputCol="rawFeatures")

# Compute Inverse Document Frequency
idf = IDF(inputCol="rawFeatures", outputCol="features")

# Create a pipeline
pipeline = Pipeline(stages=[tokenizer, hashingTF, idf])

# Fit the pipeline to your data
model = pipeline.fit(df)

# Transform the data
result = model.transform(df)

# Show the resulting DataFrame
result.show()

# Stop the Spark session
spark.stop()
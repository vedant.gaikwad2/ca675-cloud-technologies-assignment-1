# CA675 Cloud Technologies Assignment 1
# Spam detection system

The aim of this assignment is to design and develop a simple and rudimentary spam detection
system using the following technologies:

- Cloud Infrastructure using AWS, GCP or other industry-standard public cloud
- Hadoop
- MapReduce
- Hive
- Pig

A detailed description of the tasks is provided below.
**NOTE: Each student is required to submit an individual report as part of the submission.**

# Tasks

## Task 1: Cloud Infrastructure Setup (AWS, GCP, Azure, …)
### Task 1.1: Install Hadoop and create a Hadoop cluster

Feel free to create a multi-node cluster setup, a single-node cluster is the minimum requirement. This setup should be created using a public cloud system not limited to Google Cloud Platform,
Amazon Web Services or Microsoft Azure. While a Docker-based Installation on the local machine (Lab machines, PC or laptops) is possible, it is not recommended.

### Task 1.2: Install MapReduce, Pig and Hive to use the cluster created in Task 1.1

Successful installation of all three above-mentioned technologies over the cloud system from Task
1 is mandatory.

## Task 2: Dataset

### Task 2.1: Choose a relevant dataset (should be justified)

Use any public dataset repository of your choice (for example: [Kaggle.com](http://kaggle.com/), [Data.gov](http://data.gov/),
[datahub.io](http://datahub.io/), …) to explore the existing datasets. A valid and active URL link to the dataset
should be provided.

- The dataset should be fairly large and complex. You can combine multiple datasets if necessary
(not mandatory), if the datasets are combined, the combined dataset should be made publicly
available (provide URL link).
- The final dataset should described clearly and the choice of dataset should be justified.

### Task 2.2: Get data from any public dataset repository

In case the data is extracted from any website using an API (live twitter data using twitter API or
stackoverflow data stackexchange API etc), the process should be clearly explained including the
specific queries used to extract the data. Avoid using web scraper tools.

### Task 2.3: Load data into chosen cloud technology (AWS, GCP, Azure, …)

Depending on your chosen technology/system there is a number of data loading options (S3, HDFS/scoop, pig, hive etc..). You should document your data loading process in detail with appropriate screenshots and terminal commands as required.

## Task 3: Clean and process the data using Pig and/or Hive

Use the standard data cleaning and processing methods over your data considering the topic of
the assignment. For example, if the dataset contains URLs, clear or reformat the UTF encodings.
Beware of what might appear repeated data, could be reiterated information, removing such
entries will result in corrupted dataset.
Provide with explanation, the queries (PigLain/HQL/SQL etc..) used for each step of cleaning and
processing your dataset.

## Task 4: Ham and Spam using Pig and/or Hive

Classifying Ham vs Spam: The purpose of this module is to get to grips with using cloud technologies, not data analytics. Depending on your dataset you may have to provide a classification determining if something is HAM vs SPAM. A simple rule based on your dataset can be used to accomplish this. The rule you used should be present in your report. 

### Task 4.1: Query processed data to differentiate ham and spam part of the dataset

Please use the following link to the article describing the difference between ham and spam:
https://blog.barracuda.com/2013/10/03/ham-v-spam-whats-the-difference/
Design queries to create two separate datasets from the existing dataset: one
ham dataset and one spam dataset. You can use any existing “Bag of Words” for ham and spam
keywords to compare with the body and subject of the email/post.
Describe the steps taken and explain the queries used in detail.

### Task 4.2: Find the top 10 spam accounts

Detail the query used to find the top 10 spam accounts based on the number of spam posts/
emails present in the dataset

### Task 4.3: Find the top 10 ham accounts

Detail the query used to find the top 10 ham accounts based on the number of ham posts/emails
present in the dataset

## Task 5: TF-IDF using MapReduce

Term Frequency - Inverse Document Frequency is a statistical measure used to quantify the
importance of a word in a dataset.
Please use the following links to understand the concept of TF-IDF in detail:
https://www.capitalone.com/tech/machine-learning/understanding-tf-idf/http://www.tfidf.com/

### Task 5.1:

Use MapReduce to calculate the TF-IDF of the top 10 spam keywords for each top
10 spam accounts

### Task 5.2:

Use MapReduce to calculate the TF-IDF of the top 10 ham keywords for each top
10 ham accounts

# Important notes

1. The aim of this assignment is evaluate your knowledge of the technologies (Hadoop,
MapReduce, Pig, Hive). Accuracy of your system is not as important as efficient use of the
technologies. Appropriate marks will be awarded for proper use of the technologies even with
lowest demonstrated accuracy of your system.
2. Many examples, source code and tutorials are available online, use them only for reference.
All work should be original and your own work. Any beach of the university plagiarism policy
will incur an appropriate penalty. For more information, please refer to [https://www.dcu.ie/sites/default/files/finance_editor/2023-09/1 - academic_integrity_policy_v5.0_final.pdf](https://www.dcu.ie/sites/default/files/finance_editor/2023-09/1%20-%20academic_integrity_policy_v5.0_final.pdf)
3. Task 4 and Task 5 could be attempted in any order. You can use the TF-IDF results to detect
the spam and ham posts/emails.
4. A clear (but brief) description for each task in addition to the necessary screenshots, source
code, scripts and queries should be present in the report.
5. Make sure all links included in the report are active and all necessary access is provided.

# Submission

- The deliverable for this assignment is one report only in docx or pdf format and should be
submitted on Loop. **Only one document per student is allowed.**
- Submission open: Week 6, 19-October-2022
- Submission due: Week 10, 16-November-2022 (11:59pm)
- Worth 50% of the final marks

Use Gitlab (http://gitlab.computing.dcu.ie/) to create a new repository, use this repository to store your source code, scripts and/or queries for each task. The
URL link for your repository should be present in your report. Please provide access to michael.scriney@dcu.ie

# Format of the report

The format of the report document (10 pages maximum, excluding the screenshots):

1. Student details
a. Name
b. Student ID
c. Email
2. Link for the Git repository.
3. Short description of the dataset acquired (and link) along with the steps taken to acquire the
dataset.
4. Description of the steps taken to achieve each task along with query or code (link to the files
on gitlab/github etc..).
5. Snippet of the important part of the source code (wherever possible).
a. No need to include complete code in the report. Provide the repository link for the
code file instead.
6. Relevant screenshots (describe what each the screenshot represents) to show the work and
completion of the tasks (use Appendix at the end).

